package datastructures

import "testing"
import "bytes"

func TestPut(t *testing.T) {
	tree := &Tree{}
	var found bool
	var deleted bool
	var result []byte
	var numBytes int

	tree.Put("key1", []byte("value1"), false)
	found, deleted, result = tree.Get("key1")
	if !found || deleted || !bytes.Equal(result, []byte("value1")) {
		t.Errorf("error retrieving key1; value returned was: %s", result)
	}

	numBytes = tree.GetNumBytes()
	if numBytes != 10 {
		t.Errorf("incorrect number of bytes for tree: %d, should be 10", numBytes)
	}

	tree.Put("key1", []byte("the data won't go in to tree"), true)
	found, deleted, _ = tree.Get("key1")
	if !found {
		t.Errorf("couldn't retrieve deleted key1")
	}
	if !deleted {
		t.Errorf("key1 not marked as deleted")
	}

	numBytes = tree.GetNumBytes()
	if numBytes != 4 {
		t.Errorf("incorrect number of bytes for tree: %d, should be 4", numBytes)
	}

	tree.Put("key1", []byte("value1b"), false)
	found, deleted, result = tree.Get("key1")
	if !found || deleted || !bytes.Equal(result, []byte("value1b")) {
		t.Errorf("error retrieving key1. Value returned was: %s", result)
	}

	numBytes = tree.GetNumBytes()
	if numBytes != 11 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 11", numBytes)
	}

	tree.Put("key2", []byte("value2"), true)
	found, deleted, _ = tree.Get("key2")
	if !found {
		t.Errorf("couldn't retrieve deleted key2")
	}
	if !deleted {
		t.Errorf("key2 not marked as deleted")
	}
	numBytes = tree.GetNumBytes()
	if numBytes != 15 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 15", numBytes)
	}

	tree.Put("key6", []byte("value6"), false)
	tree.Put("key4", []byte("value4"), false)
	tree.Put("key0", []byte("value0"), false)
	tree.Put("key5", []byte("value5"), false)

	numBytes = tree.GetNumBytes()
	if numBytes != 55 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 55", numBytes)
	}

	found, deleted, result = tree.Get("key0")
	if !found || deleted || !bytes.Equal(result, []byte("value0")) {
		t.Errorf("error retrieving key0; value returned was: %s", result)
	}

	found, deleted, result = tree.Get("key4")
	if !found || deleted || !bytes.Equal(result, []byte("value4")) {
		t.Errorf("error retrieving key4; value returned was: %s", result)
	}

	found, deleted, result = tree.Get("key5")
	if !found || deleted || !bytes.Equal(result, []byte("value5")) {
		t.Errorf("error retrieving key5; value returned was: %s", result)
	}

	found, deleted, result = tree.Get("key6")
	if !found || deleted || !bytes.Equal(result, []byte("value6")) {
		t.Errorf("error retrieving key6; value returned was: %s", result)
	}

	tree.Put("key4", []byte("value4b"), false)
	found, deleted, result = tree.Get("key4")
	if !found || deleted || !bytes.Equal(result, []byte("value4b")) {
		t.Errorf("error retrieving key4; value returned was: %s", result)
	}

	numBytes = tree.GetNumBytes()
	if numBytes != 56 {
		t.Errorf("incorrect number of bytes for tree: %d, should be 56", numBytes)
	}

	tree.Put("key4", nil, true)
	found, deleted, _ = tree.Get("key4")
	if !found {
		t.Errorf("couldn't retrieve deleted key4")
	}
	if !deleted {
		t.Errorf("key4 not marked as deleted")
	}
	numBytes = tree.GetNumBytes()
	if numBytes != 49 {
		t.Errorf("incorrect number of bytes for tree: %d, should be 49", numBytes)
	}

	tree.Put("key4", []byte("£"), false)
	found, deleted, result = tree.Get("key4")
	if !found || deleted || !bytes.Equal(result, []byte("£")) {
		t.Errorf("error retrieving key4; value returned was: %s", result)
	}

	numBytes = tree.GetNumBytes()
	if numBytes != 51 {
		t.Errorf("incorrect number of bytes for tree: %d, should be 51", numBytes)
	}
}

func TestGet(t *testing.T) {
	tree := &Tree{}

	found, deleted, result := tree.Get("key1")

	if found {
		t.Errorf("got a value for non-existant key1: %s", result)
	}
	if deleted {
		t.Errorf("key1 never existed but was marked as deleted")
	}

	tree.Put("key1", []byte("value1"), false)
	tree.Put("key2", []byte("value2"), false)

	found1, deleted1, result1 := tree.Get("key1")
	found2, deleted2, result2 := tree.Get("key2")
	if !found1 || deleted1 || !bytes.Equal(result1, []byte("value1")) {
		t.Errorf("error retrieving key1; value returned was: %s", result1)
	}

	if !found2 || deleted2 || !bytes.Equal(result2, []byte("value2")) {
		t.Errorf("error retrieving key2; value returned was: %s", result2)
	}
}

func TestGetAll(t *testing.T) {
	tree := &Tree{}

	keys0, values0, deleted0 := tree.GetAll()
	if len(keys0) != 0 {
		t.Errorf("Got a non-empty array of keys from an empty tree: %v", keys0)
	}

	if len(values0) != 0 {
		t.Errorf("Got a non-empty array of data from an empty tree: %v", values0)
	}

	if len(deleted0) != 0 {
		t.Errorf("Got a non-empty array of delete status from an empty tree: %v", deleted0)
	}

	tree.Put("a", []byte{1}, false)
	tree.Put("c", []byte{3}, false)
	tree.Put("b", []byte{2}, false)
	tree.Put("d", []byte{4}, false)
	tree.Put("f", []byte{6}, false)
	tree.Put("e", []byte{5}, false)
	tree.Put("g", []byte{7}, false)
	tree.Put("h", []byte{8}, false)

	keys, values, deletes := tree.GetAll()
	comparekeys(t, keys, []string{"a", "b", "c", "d", "e", "f", "g", "h"})
	comparevalues(t, values, [][]byte{[]byte{byte(1)}, []byte{byte(2)}, []byte{byte(3)}, []byte{byte(4)}, []byte{byte(5)}, []byte{byte(6)}, []byte{byte(7)}, []byte{byte(8)}})
	comparedeletes(t, deletes, []bool{false, false, false, false, false, false, false, false})

	tree.Put("b", nil, true)
	tree.Put("h", nil, true)
	tree.Put("e", nil, true)

	keys1, values1, deletes1 := tree.GetAll()
	comparekeys(t, keys1, []string{"a", "b", "c", "d", "e", "f", "g", "h"})
	comparevalues(t, values1, [][]byte{[]byte{byte(1)}, nil, []byte{byte(3)}, []byte{byte(4)}, nil, []byte{byte(6)}, []byte{byte(7)}, nil})
	comparedeletes(t, deletes1, []bool{false, true, false, false, true, false, false, true})
}

func TestExists(t *testing.T) {
	tree := &Tree{}

	if tree.Exists("key1") {
		t.Errorf("Non-existant key exists in the tree!")
	}

	tree.Put("key1", []byte("value1"), false)

	if !tree.Exists("key1") {
		t.Errorf("Key1 missing from tree!")
	}

	tree.Put("key2", []byte("value2"), false)

	if !tree.Exists("key2") {
		t.Errorf("Key2 missing from tree")
	}

	tree.Put("key1", nil, true)

	if tree.Exists("key1") {
		t.Errorf("Key1 exists but was deleted")
	}

	tree.Put("key1", []byte("value1b"), false)

	if !tree.Exists("key1") {
		t.Errorf("Key1 missing from tree!")
	}
}

func TestGetNumBytes(t *testing.T) {
	tree := &Tree{}

	var result = tree.GetNumBytes()
	if result != 0 {
		t.Errorf("Incorrect number of bytes for empty tree: %d", result)
	}

	tree.Put("key1", []byte("6bytes"), false) // adding 10
	result = tree.GetNumBytes()
	if result != 10 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 10", result)
	}

	tree.Put("key2", []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, false) // adding 14
	result = tree.GetNumBytes()
	if result != 24 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 24", result)
	}

	tree.Put("£", []byte{}, false) // adding 2 (Pound character uses 2 bytes)
	result = tree.GetNumBytes()
	if result != 26 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 26", result)
	}

	tree.Put("key1", []byte{1, 2}, false) // removing 4 bytes
	result = tree.GetNumBytes()
	if result != 22 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 22", result)
	}

	tree.Put("key2", nil, true) // removing 10 bytes
	result = tree.GetNumBytes()
	if result != 12 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 12", result)
	}

	tree.Put("key2", []byte{1}, false) // adding 1 byte
	result = tree.GetNumBytes()
	if result != 13 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 13", result)
	}

	tree.Put("£", []byte{1}, false) // adding 1 byte
	result = tree.GetNumBytes()
	if result != 14 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 14", result)
	}

	tree.Put("key1", nil, true) // removing 2 bytes
	result = tree.GetNumBytes()
	if result != 12 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 12", result)
	}

	tree.Put("key2", nil, true) // removing 1 byte
	result = tree.GetNumBytes()
	if result != 11 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 11", result)
	}

	tree.Put("£", nil, true) // removing 1 byte
	result = tree.GetNumBytes()
	if result != 10 {
		t.Errorf("Incorrect number of bytes for tree: %d, should be 10", result)
	}

	tree.Put("£", nil, true) // removing nothing
	stillExists := tree.Exists("£")
	if stillExists {
		t.Errorf("key=£ exists but was already deleted!")
	}
	result = tree.GetNumBytes()
	if result != 10 {
		t.Errorf("Incorrect number of bytes for tree with all keys deleted: %d, should be size of all (deleted) keys: 10", result)
	}
}

func comparekeys(t *testing.T, actual, expected []string) {
	if len(expected) != len(actual) {
		t.Errorf("Got incorrect number of expected keys from tree: %d != %d", len(expected), len(actual))
		return
	}

	for i := 0; i < len(actual); i++ {
		if expected[i] != actual[i] {
			t.Errorf("Incorrect key at position %d: actual %s != expected %s", i, actual[i], expected[i])
		}
	}
}

func comparevalues(t *testing.T, actual, expected [][]byte) {
	if len(expected) != len(actual) {
		t.Errorf("Got incorrect number of expected values from tree: %d != %d", len(expected), len(actual))
		return
	}

	for i := 0; i < len(actual); i++ {
		if !bytes.Equal(actual[i], expected[i]) {
			t.Errorf("Incorrect data at position %d: actual %v != expected %v", i, actual[i], expected[i])
		}
	}
}

func comparedeletes(t *testing.T, actual, expected []bool) {
	if len(expected) != len(actual) {
		t.Errorf("Got incorrect number of expected deleted statuses from tree: %d != %d", len(expected), len(actual))
		return
	}

	for i := 0; i < len(actual); i++ {
		if actual[i] != expected[i] {
			t.Errorf("Incorrect data at position %d: actual %v != expected %v", i, actual[i], expected[i])
		}
	}
}
