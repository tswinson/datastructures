package datastructures

// Tree is a binary search tree.
type Tree struct {
	root       *node
	size       int
	numBytes   int
	numDeleted int
}

type node struct {
	key     string
	value   []byte
	left    *node
	right   *node
	deleted bool
}

// Put acts like an upsert. It inserts a key/value/delete status triple if the key was not already present in the tree.
// It updates the value/delete status if the key was already in the tree.
func (t *Tree) Put(key string, value []byte, delete bool) {
	if t.root == nil {
		t.root = &node{key: key, value: value, deleted: delete, left: nil, right: nil}
		t.size++
		t.numBytes += len(key) + len(value)
		return
	}

	previouslyExisted, sizeOfOldValue := t.root.put(key, value, delete)
	if previouslyExisted {
		t.numBytes -= sizeOfOldValue
	} else {
		t.numBytes += len(key)
		t.size++
	}

	if !delete {
		t.numBytes += len(value)
	}
}

// returns whether the key already existed (deleted or not) and the previous value size before updating
func (n *node) put(key string, value []byte, delete bool) (bool, int) {
	if key == n.key {
		sizeOfOldValue := len(n.value)
		n.deleted = delete
		n.value = value
		if delete {
			n.value = nil
		}
		return true, sizeOfOldValue
	} else if key < n.key {
		if n.left == nil {
			n.left = &node{key: key, value: value, deleted: delete}
			return false, 0
		}
		return n.left.put(key, value, delete)
	} else {
		if n.right == nil {
			n.right = &node{key: key, value: value, deleted: delete}
			return false, 0
		}
		return n.right.put(key, value, delete)
	}

}

// Get returns a boolean indicating whether the key was found,
// whether the key was deleted, and the value associated with the key if it was not deleted.
func (t *Tree) Get(k string) (bool, bool, []byte) {
	return t.root.get(k)
}

func (n *node) get(k string) (bool, bool, []byte) {
	if n == nil {
		return false, false, nil
	} else if k == n.key {
		return true, n.deleted, n.value
	} else if k < n.key {
		return n.left.get(k)
	} else {
		return n.right.get(k)
	}
}

// GetAll returns all keys, values, and deletion status from the tree in sorted order (by key).
// It returns threww aligned arrays, the first of which contains the keys and the second contains the values,
// and the third contains the deletion status.
func (t *Tree) GetAll() ([]string, [][]byte, []bool) {
	keys := make([]string, t.size+t.numDeleted)
	values := make([][]byte, t.size+t.numDeleted)
	deleted := make([]bool, t.size+t.numDeleted)
	t.root.getAll(0, keys, values, deleted)
	return keys, values, deleted
}

func (n *node) getAll(position int, keys []string, values [][]byte, deleted []bool) int {
	if n == nil {
		return position
	}

	lastPosition := n.left.getAll(position, keys, values, deleted)

	// var nextPosition = lastPosition

	keys[lastPosition] = n.key
	values[lastPosition] = n.value
	deleted[lastPosition] = n.deleted
	// lastPosition++

	return n.right.getAll(lastPosition+1, keys, values, deleted)
}

// Exists returns whether a key is present and not deletd in the tree.
func (t *Tree) Exists(k string) bool {
	found, deleted, _ := t.root.get(k)
	return found && !deleted
}

// GetNumBytes returns the total number of bytes in all keys and values in the tree,
// including deleted keys.
func (t *Tree) GetNumBytes() int {
	return t.numBytes
}
